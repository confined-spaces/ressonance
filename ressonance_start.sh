# Bash start script

if [ ! -d public/ressonance/include ]
then
    # Create library files if not already there.
    mkdir public/ressonance/include
    cp node_modules/factorygame/include/* public/ressonance/include/factorygame -r
    cp node_modules/three public/ressonance/include -r
fi

# Compile typescript files.
npx tsc