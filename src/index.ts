import { Actor, GameplayUtilities, GameplayStatics, GameEngine } from "/ressonance/include/factorygame/factorygame.js"

class RandomProgressTicker extends Actor {
    private progress: number = 0.25;
    private increaseRate: number = 1.5;
    private timeSinceSpawned: number = 0;

    public tick(deltaTime: number): void {
        this.timeSinceSpawned += deltaTime;
        this.progress += deltaTime * this.increaseRate * Math.random();
        if (this.progress > 1) {
            // Loop back to 0 when bar reached the end.
            this.progress = 0;
        }

        if (this.timeSinceSpawned % (0.3 * Math.random()) <= deltaTime) {
            // Apply to the progress bar on the page infrequently.
            let newVal = `${this.progress * 100}%`;
            $(".progress-bar").css("width", newVal);
        }
    }
}

// Create the game engine and spawn a ProgressTicker dynamically.
GameplayUtilities.createGameEngine(GameEngine);
// This game engine usage is split up between this file and telescope.ts
// so that this can load fast to display the animated progress bar.
// Telescope.ts requires many things to load, such as Three.js and the
// 3d model of the telescope itself, which takes some time and would delay
// the loading of the progress bar animation.
var progressTicker = GameplayStatics.world.spawnActor(RandomProgressTicker, [0, 0]);

