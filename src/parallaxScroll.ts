function mapRange(v, a, b, c, d) {
    return c + (d - c) * ((v - a) / (b - a));
}

function mapRangeClamped(v, a, b, c, d) {
    if (c > d) {
        return Math.min(c, Math.max(d, mapRange(v, a, b, c, d)))
    }
    return Math.min(d, Math.max(c, mapRange(v, a, b, c, d)))
}

function getScrollPercent(el) {
    el = el || this;
    let now = window.pageYOffset;
    let start = el.offsetTop - el.scrollTop + el.clientTop;
    let end = start + el.clientHeight;
    return mapRange(now, start, end, .5, 1);
}

$(window).on("scroll", () => {
    $(".parallax--background").each(function () {
        const el = this;
        const scrollPercent = getScrollPercent(el);
        const yPercent = mapRange(scrollPercent, 0, 1, 160, -60); // EXTREME!
        //const yPercent = mapRange(scrollPercent, 0, 1, 70, 30);
        el.style["background-position"] = `50% ${yPercent}%`;
    });
});