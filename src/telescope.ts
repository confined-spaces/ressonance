import { FBXLoader } from "/ressonance/include/three/examples/jsm/loaders/FBXLoader.js";
import { Actor, Loc, GameplayStatics, MathStat, GameplayUtilities, GameEngine } from "/ressonance/include/factorygame/factorygame.js";
var THREE = window["THREE"];
var progressTicker = window["progressTicker"]

let scene: THREE.Scene;
let canvas: HTMLCanvasElement;
let camera: THREE.PerspectiveCamera;
let renderer: THREE.WebGLRenderer;

class Telescope extends Actor {
    /** Telescope mesh created from FBX model. */
    telescopeMesh: THREE.Object3D = null;

    /** 3d world position of telescope last frame. */
    lastPos: Loc = null;
    /** Rotation of X axis of telescope last frame. */
    lastRot: number = null;

    constructor() {
        super();

        // Create THREE.js scene, camera and renderer.
        scene = new THREE.Scene();
        canvas = document.getElementById("telescope-canvas") as HTMLCanvasElement;
        camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

        // Create the webGL renderer.
        renderer = new THREE.WebGLRenderer({ antialias: true, canvas: canvas, alpha: true });
        renderer.setClearColor(0, 0);
        //scene.background = new THREE.Color(0);
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.shadowMap.enabled = true;

        // Resize to fit whole window always.
        renderer.setSize(window.innerWidth, window.innerHeight);
        window.onresize = () => {
            camera.aspect = window.innerWidth / window.innerHeight;
            camera.updateProjectionMatrix();
            renderer.setSize(window.innerWidth, window.innerHeight);
        };

        // Add lights
        let dirLight = new THREE.DirectionalLight();
        dirLight.rotation.x = Math.PI / 2;
        scene.add(dirLight);

        let envLight = new THREE.AmbientLight(0xffffff);
        scene.add(envLight);

        // TODO: import FBX model
        let fbxLoader = new FBXLoader();
        $("#goto-telescope").hide()
        fbxLoader.load("./content/telescope.fbx", (object) => {
            scene.add(object);
            object.scale.set(.2, .2, .2);
            // Will be determined by scroll position.
            object.position.y = 10;
            this.telescopeMesh = object;

            // Stop randomly ticking!
            GameplayStatics.world.destroyActor(progressTicker);
            // Hide loading progress bar at 100% full.
            $(".progress-bar").css("width", "100%");
            setTimeout(() => $(".progress").fadeOut("slow"), 500);
            setTimeout(() => $("#goto-telescope").addClass("active").fadeIn("slow"), 1000);
        });
        camera.position.set(0, 20, 30);
    }

    tick(deltaTime: number): void {
        let scrollTop = window.pageYOffset;
        // canvas.height not accurate on mobile. THREE.js will set its
        // true height using inline style.
        let canvasHeight = Number($(canvas).css("height").replace("px", ""));
        // 0% = top of canvas at bottom of screen.
        // 100% = bottom of canvas at top of screen.
        let scrollCanvasPercent = scrollTop / (canvasHeight * 2);

        if (this.telescopeMesh != null) {
            // MathStat.lerp currently broken for numbers :(
            let lerp1 = (a, b, x) => a + (b - a) * x;

            let newPos: Loc;
            let newRot: number;
            newRot = lerp1(Math.PI / 3 * .2, Math.PI / 2, scrollCanvasPercent);
            let quickSlideThreshold = .7;
            if (scrollCanvasPercent < quickSlideThreshold) {
                newPos = MathStat.lerp(new Loc(0, 5, 0), new Loc(0, 15, 20), scrollCanvasPercent);
            } else {
                scrollCanvasPercent = MathStat.clamp((scrollCanvasPercent - quickSlideThreshold) * (1 / (1 - quickSlideThreshold)));
                newPos = MathStat.lerp(new Loc(0, 15, 20), new Loc(0, 25, 50), scrollCanvasPercent);
            }

            // Apply smooth interpolation to new point.
            if (this.lastPos !== null) {
                newPos = MathStat.lerp(this.lastPos, newPos, .1);
            }
            this.lastPos = newPos;
            if (this.lastRot !== null) {
                newRot = lerp1(this.lastRot, newRot, .1);
            }
            this.lastRot = newRot;

            // @ts-ignore
            this.telescopeMesh.position.set(...newPos);
            this.telescopeMesh.rotation.x = newRot;
        }
        renderer.render(scene, camera);
    }
}

// Create default game engine if not already created.
if (!GameplayStatics.isGameValid()) {
    GameplayUtilities.createGameEngine(GameEngine);
}

// Create the telescope actor in the world.
let telescopeActor = GameplayStatics.world.spawnActor(Telescope, [0, 0]);