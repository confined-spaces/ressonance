# Powershell start script

if (-not (Test-Path "public/ressonance/include")) {
    # Create library files if not already there.
    New-Item -Path public/ressonance/include -Type Directory
    Copy-Item node_modules/factorygame/include/* public/ressonance/include -Recurse
    Copy-Item node_modules/three public/ressonance/include -Recurse
}

# Compile typescript files.
npx tsc